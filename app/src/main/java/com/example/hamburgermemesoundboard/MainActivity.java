package com.example.hamburgermemesoundboard;

import androidx.appcompat.app.AppCompatActivity;

import android.media.MediaDataSource;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {


    MediaPlayer mediaPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ImageView hamburger = findViewById(R.id.hamburger);
        ImageView hamhamham = findViewById(R.id.hamhamham);
        ImageView burburbur = findViewById(R.id.burburbur);
        ImageView gergerger = findViewById(R.id.gergerger);
        ImageView hambur = findViewById(R.id.hambur);
        ImageView burgerham = findViewById(R.id.burgerham);
        ImageView hamger = findViewById(R.id.hamger);
        ImageView gerberger = findViewById(R.id.gerberger);
        ImageView u = findViewById(R.id.u);
        ImageView uuuuuuu = findViewById(R.id.uuuuuuu);


        hamburger.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               startMediaPlayer(getApplicationContext(), R.raw.hamburger);
            }
        });

        hamhamham.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startMediaPlayer(getApplicationContext(), R.raw.hamhamham);

            }
        });
        burburbur.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startMediaPlayer(getApplicationContext(), R.raw.burburbur);

            }
        });
        gergerger.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startMediaPlayer(getApplicationContext(), R.raw.gergerger);
            }
        });
        hambur.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startMediaPlayer(getApplicationContext(), R.raw.hambur);
            }
        });
        burgerham.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startMediaPlayer(getApplicationContext(), R.raw.burgurham);
            }
        });

        hamger.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startMediaPlayer(getApplicationContext(), R.raw.hamger);
            }
        });

        gerberger.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startMediaPlayer(getApplicationContext(), R.raw.gerburgur);
            }
        });

        u.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startMediaPlayer(getApplicationContext(), R.raw.u);
            }
        });

        .setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startMediaPlayer(getApplicationContext(), R.raw.);
            }
        });


    }


    private void startMediaPlayer(Context context, int soundID) {
        try {
            if (soundID != null) {
                if (mediaPlayer != null) {
                    mediaPlayer.reset();
                }

                mediaPlayer = MediaPlayer.create(context, soundID);
                mediaPlayer.start();
            }
        } catch (Exception e) {
            System.out.println("stardMediaPlayer Method:" + e.getMessage());
        }

    }




}